terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
}

provider "cloudflare" {
  email   = var.cloudflare_email
  api_key = var.cloudflare_api_key
}

resource "cloudflare_filter" "block_urls_with_blockme" {
  zone_id = var.zone_id
  expression = "http.request.uri.path contains \"blockme\""
  description = "Block all URLs containing 'blockme'"
}

resource "cloudflare_firewall_rule" "block_urls_with_blockme_rule" {
  zone_id = var.zone_id
  filter_id = cloudflare_filter.block_urls_with_blockme.id
  action    = "block"
  description = "Block all URLs containing 'blockme'"
  paused = false
  priority = 1
}

resource "cloudflare_filter" "block_urls_with_juan" {
  zone_id = var.zone_id
  expression = "http.request.uri.path contains \"juan2\""
  description = "Block all URLs containing 'juan2'"
}

resource "cloudflare_firewall_rule" "block_urls_with_juan_rule" {
  zone_id = var.zone_id
  filter_id = cloudflare_filter.block_urls_with_juan.id
  action    = "block"
  description = "Block all URLs containing 'juan'"
  paused = false
  priority = 1
}

