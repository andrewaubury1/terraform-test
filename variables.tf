variable "cloudflare_email" {
  description = "Email for Cloudflare account"
  type        = string
}

variable "cloudflare_api_key" {
  description = "API key for Cloudflare account"
  type        = string
}

variable "zone_id" {
  description = "Zone ID for the Cloudflare zone"
  type        = string
}


